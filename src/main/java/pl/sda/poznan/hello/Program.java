package pl.sda.poznan.hello;

public class Program {

    //var args example
    public static String greet(String... names) {


        String hello = "Hello";
        //if there's no name, say generally
        if (names == null) {
            return "Hello, my friend.";
        }
        //if name is capital case, then shout
        boolean allNamesUpperCase = true;
        String[] upperCaseNames = new String[names.length];
        int numberOfUpperCaseNames = 0;
        for (String name : names) {
            if (name.equals(name.toUpperCase())) {
                upperCaseNames[numberOfUpperCaseNames++] = name;
            } else {
                allNamesUpperCase = false;
            }
        }
        if (allNamesUpperCase) {
            hello = hello.toUpperCase();
        }
        //return greeting
        if (names.length == 1) {
            return hello + ", " + names[0] + ".";
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append(hello);
            for (int i = 0; i < names.length - 1; i++) {
                if (names[i] != null) {
                    builder.append(", ").append(names[i]);
                }
            }
            builder.append(" and ").append(names[names.length - 1]).append(".");

            return builder.toString();
        }
    }
}
