package pl.sda.poznan.mini_shop.model;

import pl.sda.poznan.mini_shop.iterator.Iterable;
import pl.sda.poznan.mini_shop.iterator.Iterator;

import java.util.ArrayList;
import java.util.List;

public class ShopCart implements Iterable<CartItem> {

    private List<CartItem> cartItems = new ArrayList<>();
    private double sum;

    /**
     * Add cart item to cart and actualize price sum
     *
     * @param cartItem
     */
    public void add(CartItem cartItem) {
        this.cartItems.add(cartItem);
        sum += cartItem.getUnitPrice() * cartItem.getQuantity();
    }

    public double getSum() {
        return sum;
    }

    @Override
    public Iterator<CartItem> getIterator() {
        return new Iterator<CartItem>() {

            private int index = 0;

            @Override
            public boolean hasNext() {
                return index < cartItems.size();
            }

            @Override
            public CartItem next() {
                return cartItems.get(index++);
            }

            @Override
            public void reset() {
                index = 0;
            }
        };
    }

}
