package pl.sda.poznan.mini_shop.file;

import pl.sda.poznan.mini_shop.model.Product;

import java.io.IOException;
import java.util.List;

public interface FileWriter<E> {
    void saveToFile(List<E> elements) throws IOException;
}
