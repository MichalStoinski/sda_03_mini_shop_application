package pl.sda.poznan.mini_shop.file.product;

import pl.sda.poznan.mini_shop.model.Product;

import java.io.IOException;
import java.util.List;

public class JsonProductWriter implements ProductFileWriter {

    private String path;

    public JsonProductWriter(String path) {
        this.path = path;
    }

    @Override
    public void saveToFile(List<Product> elements) throws IOException {
        throw new UnsupportedOperationException();
    }
}
