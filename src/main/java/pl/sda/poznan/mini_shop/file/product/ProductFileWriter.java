package pl.sda.poznan.mini_shop.file.product;

import pl.sda.poznan.mini_shop.file.FileWriter;
import pl.sda.poznan.mini_shop.model.Product;

/**
 * Create to choose generic type as Product
 */
public interface ProductFileWriter extends FileWriter<Product> {
}
