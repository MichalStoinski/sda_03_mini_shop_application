package pl.sda.poznan;

import org.junit.Before;
import org.junit.Test;
import pl.sda.poznan.mini_shop.model.Product;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.*;

public class LambdaExpressionTest {

    private List<Product> products;

    // playground
    @Before
    public void init() {
        Product smartphone = new Product("Nokia", "Nokia 3310", 150D);
        Product laptop = new Product("Dell", "Ultrabook", 1500D);
        Product concole = new Product("Xbox", "One X", 2000D);

        this.products = Arrays.asList(smartphone, laptop, concole);
    }

    @Test
    public void test() {
        // filter with lambda syntax
        List<Product> result = products
                .stream()
                .filter(product -> product.getPrice() > 1000)
                .collect(Collectors.toList());

        assertEquals(2, result.size());
        assertEquals(3, products.size());
    }

    @Test
    public void should_find_where_starts_with_given_letters() {
        // filter with lambda syntax
        List<Product> productList = products
                .stream()
                .filter(product -> product.getPrice() > 1000)
                .filter(product -> product.getName().startsWith("X"))
                .collect(Collectors.toList());

        assertEquals(1, productList.size());
    }

    @Test
    /**
     *  find first costs less than 2000
     */
    public void should_Find_first() {

/*
        Product result = this.products
                .stream()
                .filter(product -> product.getPrice() < 2000)
                .collect(Collectors.toList())
                .get(0);
*/

        Optional<Product> productOptional = this.products
                .stream()
                .filter(product -> product.getPrice() < 2000)
                .findFirst();

//        Product product = productOptional.orElseThrow(() -> new NoSuchElementException());
        Product product = productOptional.orElseThrow(NoSuchElementException::new);
    }

    @Test
    /**
     *  sort alphabetically
     */
    public void should_sort_by_name() {
//        this.products.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
        this.products.sort(Comparator.comparing(Product::getName));
    }

    /**
     * foreach product get its name to list
     */
    @Test
    public void should_map_to_name() {
        List<String> list = this.products
                .stream()
                .map(Product::getName)
                .collect(Collectors.toList());

        assertEquals(3, list.size());
    }
}