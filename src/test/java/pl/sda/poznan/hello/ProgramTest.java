package pl.sda.poznan.hello;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProgramTest {

    @Test
    public void should_return_greetingWithName() {
        String greet = Program.greet("Jan");
        assertEquals("Hello, Jan.", greet);
    }

    @Test
    public void should_return_greetingWithoutName() {
        //TODO confusing argument 'null' unclear if a varargs or non-varargs call is desired
        String greet = Program.greet(null);
        assertEquals("Hello, my friend.", greet);
    }

    @Test
    public void should_return_greetingUppercase() {
        String greet = Program.greet("JAN");
        assertEquals("HELLO, JAN.", greet);
    }

    @Test
    public void should_return_twoGreetings() {
        String[] names = {"Jan", "Ala"};
        String greet = Program.greet(names);
        assertEquals("Hello, Jan and Ala.", greet);
    }

    @Test
    public void should_return_manyGreetings() {
        String[] names = {"Jan", "Ala", "Ula"};
        String greet = Program.greet((names));
        assertEquals("Hello, Jan, Ala and Ula.", greet);

    }
}