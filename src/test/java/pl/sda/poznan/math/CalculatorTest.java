package pl.sda.poznan.math;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {

    @Test
    public void should_return_zero_when_string_empty() {
        int sum = Calculator.sum("");
        assertEquals(0, sum);
    }

    @Test
    public void should_return_number() {
        int sum = Calculator.sum("4");
        assertEquals(4, sum);
    }

    @Test
    public void should_add_numbers(){
        int sum = Calculator.sum("4,5");
        assertEquals(9,sum);
    }
}